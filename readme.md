# Тестовое задание
## Требования:
необходимо перед началом работ установить:
- docker https://docs.docker.com/v17.12/install/ 
- docker-compose https://docs.docker.com/compose/install/
- git https://git-scm.com/downloads
- yarn https://yarnpkg.com/lang/en/docs/install/#debian-stable

## Последовательно в консоли выполните следующие команды:
- ```git clone git@gitlab.com:diamis/parser-lostfilm.git```
- ```cd parser-lostfilm``` - переходим в директорию проекта
- ```composer install``` - установка зависимостей
- ```yarn install``` - установка npm пакетов
- ```cp .env.example .env``` - скопировать файл найстроек
- ```docker-compose up -d``` - выполнить сборку контейнера
- ```docker-compose exec php bash``` - выполните вход в контейнер докера
- ```php artisan migrate``` - выполнить миграции базы данных
- ```php artisan parser``` - запускаем парсер

## В браузере перейдите на страницу:
http://localhost:8888
