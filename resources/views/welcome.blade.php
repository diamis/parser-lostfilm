<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
                font-weight: bold;
            }

            .title sup{
                font-family: Verdana, Geneva, Tahoma, sans-serif;
                font-weight: 100;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .text{
                font-family: Verdana, Geneva, Tahoma, sans-serif;
                text-align: left;
                max-width: 680px;
                margin: 60px auto 0 auto;
            }
            .text p,
            .text li{
                font-size: 15px;
                line-height: 19px;
            }

            .text ul{
                padding: 0 0 0 20px;
                margin: 0;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel <sup>5.6</sup>
                </div>
                <div class="links">
                    <a href="{{ route('movies.list') }}">Фильмы</a>
                </div>
                <div class="text">
                    <p><b>Тестовое задание CityLife:</b></p>
                    <p>Необходимо развернуть проект на фреймворке Laravel версии 5.6. В этом проекте реализовать консольный скрипт, собирающий данные о новинках кинопроката на сайте ЛостФильм (<a href="https://www.lostfilm.tv/new/">https://www.lostfilm.tv/new/</a>) и сохраняющего название сериала (русское) название серии, дату выхода серии в прокат и ссылку на страницу подробной информации/скачивания в БД (любая на выбор). Скрипт должен быть написан с учетом возможности его постановки в cron.</p>
                    <p>Также в этом проекте нужно сделать веб-страницу, выводящую постранично по 10 элементов из списка полученных сериалов, отсортированных по дате выхода (обратный порядок). И поле для поиска по названию сериала/названию серии (по вхождению строки в русское или английское название).</p>
                    <p><b>Критерии оценки:</b></p>
                    <ul>
                        <li>чистый, читаемый, структурируемый php код, объектно ориентированный дизайн</li>
                        <li>корректная схема базы данных</li>
                        <li>простота развертывание проекта</li>
                        <li>использование сторонних библиотек приветствуется</li>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>
