<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;;
            }

            a {
                display: flex;
                flex-direction: column;
                text-decoration: none;
            }
            a div{
                display: inline-block;
            }
            ul{
                display: flex;
                padding: 0;
                margin: 0 0 40px;
                list-style: none;
            }
            ul li{
                margin: 0 5px;
            }
        </style>
    </head>
    <body>
        @isset($movies)
            @foreach ($movies as $item)
                <a href="{{ $item->href }}" target="_blank">
                    <div>Название: {{ $item->name }}</div>
                    <div>Серия: {{ $item->episode }}</div>
                    <div>Дата выхода: {{ $item->date }}</div>
                </a>
                <br>
            @endforeach
            {{ $movies->links() }}
        @endisset
    </body>
</html>
