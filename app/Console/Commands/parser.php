<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Parser\LostFilm\Spider;
use Exception;

class parser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start parser LostFilm';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            Spider::start();
        } catch (Exception $e){
            $this->error($e->getMessage());
        }
    }
}
