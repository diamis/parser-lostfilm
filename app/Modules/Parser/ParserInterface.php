<?php
namespace App\Modules\Parser;

use App\Modules\Parser\Remote;

interface ParserInterface {

    public function __construct(Remote $remote);

    public function parse();
}