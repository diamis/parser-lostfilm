<?php
namespace App\Modules\Parser\LostFilm;

use App\Modules\Parser\Remote;
use App\Modules\Parser\ParserInterface;

class RowPost implements ParserInterface {

    public $remote;

    public function __construct(Remote $remote)
    {
        $this->remote = $remote;
    }

    public function parse() {
        $rows = $this->remote->dom->find('.body .row');
        $result = [];
        foreach($rows as $row) {
            $url = $this->remote->host . $row->find('a')[0]->getAttribute('href');
            $alpha = $row->find('.details-pane .alpha');
            $date = preg_replace("/[^0-9\.]/", '', $alpha[1]->text);
            
            $result[] = [
                'href' => $url,
                'name' => $row->find('.name-ru')->text,
                'episode' => $alpha[0]->text,
                'date' => \DateTime::createFromFormat('d.m.Y',$date)->format('Y-m-d')
            ];

        }
        return $result;
    }
}