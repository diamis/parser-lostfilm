<?php
namespace App\Modules\Parser\LostFilm;

use App\Movie;
use App\Modules\Parser\Remote;
use App\Modules\Parser\LostFilm;

class Spider 
{
    public static function start() {
        $page = 'https://www.lostfilm.tv/new/page_1';
        while($page) {
            echo $page . "\n";

            $remote = new Remote($page);
            $nextPage = new LostFilm\NextPage($remote);
            $rowsPost = new LostFilm\RowPost($remote);
    
            $page = $nextPage->parse();
            $rows = $rowsPost->parse();

            Movie::insert($rows);
        }
    }
}