<?php
namespace App\Modules\Parser\LostFilm;

use App\Modules\Parser\Remote;
use App\Modules\Parser\ParserInterface;

class NextPage implements ParserInterface {

    public $remote;

    public function __construct(Remote $remote)
    {
        $this->remote = $remote;
    }

    public function parse() {
        $row = $this->remote->dom->find('.pagging-pane')[0];
        $a = $row->find('a');
        $next_page = $this->remote->host . $a[count($a)-1]->getAttribute('href');

        if($next_page===$this->remote->path) return false;
        else return $next_page;
    }
}