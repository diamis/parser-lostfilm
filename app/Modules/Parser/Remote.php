<?php
namespace App\Modules\Parser;

use PHPHtmlParser\Dom;
use Exception;
use Log;

class Remote {

    public $dom;
    public $host;
    public $path;

    public function __construct($path)
    {
        try{
            $html = file_get_contents($path);
            $parseUrl = parse_url($path);
            if(isset($parseUrl))
                $this->host = $parseUrl['scheme'].'://'.$parseUrl['host'];

            $this->path = $path;
            $this->dom = new Dom;
            $this->dom->load($html);
        } catch (Exception $e){
			Log::error($e, ['exception']);
		}
    }
}