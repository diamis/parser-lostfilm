<?php

namespace App\Http\Controllers\Movies;

use Illuminate\Routing\Controller;
use App\Movie;

class BaseController extends Controller
{
    public function index() {
        $movies = Movie::query()
            ->orderBy('date', 'asc')
            ->paginate(10);
            
        return view('movies.lists', compact('movies', $movies));
    }
}
